package com.javarush.task.task25.task2506;

public class LoggingStateThread extends Thread {
    Thread thread;
    State state, lastState;

    public LoggingStateThread(Thread thread) {
        this.thread = thread;
        lastState = thread.getState();
        System.out.println(lastState);
    }

    @Override
    public void run() {
        do {
            state = thread.getState();
            if (state != lastState) {
                System.out.println(state);
                lastState = state;
            }
        }
        while (state != State.TERMINATED);
    }
}
