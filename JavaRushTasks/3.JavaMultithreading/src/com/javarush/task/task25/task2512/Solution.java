package com.javarush.task.task25.task2512;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
Живем своим умом
*/
public class Solution implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        t.interrupt();
        List<Throwable> exception = new ArrayList<>();
        exception.add(e);
        Throwable throwable;
        while ((throwable = exception.get(exception.size() - 1).getCause()) != null) {
            exception.add(throwable);
        }
        Collections.reverse(exception);
        for (Throwable el : exception) System.out.println(el);
    }

    public static void main(String[] args) {
        new Solution().uncaughtException(Thread.currentThread(), new Exception("ABC", new RuntimeException("DEF", new IllegalAccessException("GHI"))));
    }
}
