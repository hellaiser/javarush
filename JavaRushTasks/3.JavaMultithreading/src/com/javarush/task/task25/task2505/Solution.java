package com.javarush.task.task25.task2505;

/* 
Без дураков
*/
public class Solution {

    public static void main(String[] args) {
        MyThread myThread = new Solution().new MyThread("super secret key");
        myThread.start();
    }



    public class MyThread extends Thread {
        private String secretKey;

        public MyThread(String secretKey) {
            this.secretKey = secretKey;
            setUncaughtExceptionHandler(new MyUncaughtExceptionHandler());
            setDaemon(false);
        }

        private class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
            @Override
            public void uncaughtException(Thread t, Throwable e)  {
                MyThread myThread = (MyThread) t;
                String secretKey = myThread.secretKey;
                String nameThread = t.getName();
                String exception = e.getMessage();
                String out = String.format("%s, %s, %s", secretKey, nameThread, exception);
                try {
                    Thread.sleep(500);
                    System.out.println(out);
                }

                catch (InterruptedException exc) {

                }
            }
        }

        @Override
        public void run() {
            throw new NullPointerException("it's an example");
        }
    }

}

