package com.javarush.task.task21.task2113;


import java.util.ArrayList;
import java.util.List;

public class Hippodrome {
    private List<Horse> horses;
    static Hippodrome game;

    public Hippodrome(List<Horse> horses) {
        this.horses = horses;
    }

    public void run() throws InterruptedException {
        for (int i = 0; i < 100; i++){
            move();
            print();
            Thread.sleep(200);
        }
    }
    public void move() {
        for(int i = 0; i < horses.size(); i++) {
            horses.get(i).move();
        }
    }
    public void print() {
        for (int i = 0; i < horses.size(); i++) {
            horses.get(i).print();
        }

        for (int i = 0; i < 10; i++) {
            System.out.println();
        }
    }

    public Horse getWinner() {
        Horse winner = horses.get(0);

        for (int i = 1; i < horses.size(); i++) {
            if(winner.getDistance() < horses.get(i).getDistance()) winner = horses.get(i);
        }

        return winner;
    }

    public void printWinner() {
        Horse winner = getWinner();
        System.out.println("Winner is " + winner.getName() + "!");
    }

    public List<Horse> getHorses() {
        return horses;
    }

    public static void main(String[] args)  {
        List<Horse> horseList = new ArrayList<Horse>();
        horseList.add(new Horse("Strela", 3, 0));
        horseList.add(new Horse("Plamya", 3, 0));
        horseList.add(new Horse("Burya", 3, 0));

        game = new Hippodrome(horseList);

        try {
            game.run();
            game.printWinner();
        }

        catch (InterruptedException exc) {
            System.out.println(exc);
        }
    }
}
