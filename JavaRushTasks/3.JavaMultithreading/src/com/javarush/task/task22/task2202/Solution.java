package com.javarush.task.task22.task2202;

/* 
Найти подстроку
*/
public class Solution {
    public static void main(String[] args) {
        System.out.println(getPartOfString("JavaRush - лучший сервис обучения Java."));
    }

    public static String getPartOfString(String string) {
        if (string == null) throw new TooShortStringException();
        String result = "";
        String[] array = string.split(" ");
        for (int i = 1; i <= 4; i++) {
            try {
                result = result + array[i] + " ";
            }

            catch (ArrayIndexOutOfBoundsException exc) {
                throw new TooShortStringException();
            }
        }
        return result.trim();
    }

    public static class TooShortStringException extends RuntimeException{
    }
}
