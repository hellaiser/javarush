package com.javarush.task.task22.task2211;

import java.io.*;
import java.nio.charset.Charset;

/* 
Смена кодировки
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(args[0]);
        FileOutputStream fileOutputStream = new FileOutputStream(args[1]);
        Charset win1251 = Charset.forName("Windows-1251");
        Charset utf = Charset.forName("UTF-8");
        byte[] buff = new byte[fileInputStream.available()];

        fileInputStream.read(buff);
        String s = new String(buff, win1251);
        buff = s.getBytes(utf);
        fileOutputStream.write(buff);

        fileInputStream.close();
        fileOutputStream.close();
    }
}
