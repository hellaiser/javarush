package com.javarush.task.task22.task2212;

import java.util.ArrayList;

/*
Проверка номера телефона
*/
public class Solution {
    public static boolean checkTelNumber(String telNumber) {
        if(telNumber == null || telNumber.isEmpty()) return false;
        String temp = telNumber.replaceAll("\\D", "");
        if(telNumber.charAt(0) == '+') {
            if (temp.length() != 12) return false;
            else return ((telNumber.matches("\\+\\d\\d\\(\\d{3}\\)\\d{7}") || telNumber.matches("\\+\\d+\\-{1}\\d+\\-{1}\\d+") || telNumber.matches("\\+\\d{12}")) && !telNumber.matches("[^\\d+()-]"));
        }
        else if (temp.length() == 10) return (telNumber.matches("\\({1}\\d{3}\\)\\d+\\-?\\d+\\-?\\d+") || telNumber.matches("\\d+\\-?\\d+\\-?\\d+"));
        else return false;
    }

    public static void main(String[] args) {
        ArrayList<String> listTelNumber = new ArrayList<>();
        listTelNumber.add("+380501234567");
        listTelNumber.add("+38(050)1234567");
        listTelNumber.add("+38050123-45-67");
        listTelNumber.add("050123-4567");
        listTelNumber.add("+38)050(1234567");
        listTelNumber.add("+38(050)1-23-45-6-7");
        listTelNumber.add("050ххх4567");
        listTelNumber.add("050123456");
        listTelNumber.add("(0)501234567");

        for(int i = 0; i < listTelNumber.size(); i++) {
            System.out.println(checkTelNumber(listTelNumber.get(i)));
        }
    }
}
