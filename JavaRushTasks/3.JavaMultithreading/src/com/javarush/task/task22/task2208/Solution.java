package com.javarush.task.task22.task2208;

import java.util.HashMap;
import java.util.Map;

/* 
Формируем WHERE
*/
public class Solution {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();


        System.out.println(getQuery(map));
    }
    public static String getQuery(Map<String, String> params) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<String,String> pair : params.entrySet()) {
            if (pair.getValue() != null) stringBuilder = stringBuilder.append(pair.getKey()).append(" = ").append("'").append(pair.getValue()).append("'").append(" and ");
        }
        String result = "";
        if (stringBuilder.length() > 0) result = stringBuilder.substring(0, stringBuilder.length() - 5);
        return result;
    }
}
