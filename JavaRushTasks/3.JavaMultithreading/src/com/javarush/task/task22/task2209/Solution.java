package com.javarush.task.task22.task2209;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/*
Составить цепочку слов
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader fileReader = new BufferedReader(new FileReader(reader.readLine()));
        String temp;
        ArrayList<String> list = new ArrayList<>();

        while ((temp = fileReader.readLine()) != null) {
            Collections.addAll(list, temp.split(" "));
        }

        String[] words = list.toArray(new String[list.size()]);

        StringBuilder result = getLine(words);
        System.out.println(result.toString());
    }

    public static StringBuilder getLine(String... words) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (words == null || words.length == 0) return stringBuilder;
        Map<Integer, String> map = new HashMap<>();
        for (int i = 0; i < words.length; i++) {
            ArrayList<String> tempList = new ArrayList<>();
            Collections.addAll(tempList, words);
            stringBuilder.append(words[i]);
            tempList.remove(i);
            for (int j = 0; j < tempList.size();) {
                if(stringBuilder.toString().toLowerCase().charAt(stringBuilder.length() - 1) == tempList.get(j).toLowerCase().charAt(0)) {
                    stringBuilder.append(" " + tempList.get(j));
                    tempList.remove(j);
                    j = 0;
                }
                else j++;
            }
            map.put(stringBuilder.length(), stringBuilder.toString());
            stringBuilder.delete(0, stringBuilder.length());
        }

        int max = 0;
        for(Map.Entry <Integer, String> pair : map.entrySet()) {
            if(max < pair.getKey()) max = pair.getKey();
        }

        stringBuilder = new StringBuilder(map.get(max));
        return stringBuilder;
    }
}
