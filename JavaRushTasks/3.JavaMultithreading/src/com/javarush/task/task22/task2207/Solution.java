package com.javarush.task.task22.task2207;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/* 
Обращенные слова
*/
public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader fileReader = new BufferedReader(new FileReader(reader.readLine()));

        ArrayList<String> temp = new ArrayList<>();
        String tempString;

        while ((tempString = fileReader.readLine()) != null) {
            Collections.addAll(temp, tempString.split(" "));
        }

        while (temp.size() != 1) {
            int tempSize = temp.size();
            StringBuilder stringBuilder = new StringBuilder(temp.get(0));
            tempString = stringBuilder.reverse().toString();
            for (int i = 1; i < temp.size(); i++) {
                String tempString2 = temp.get(i);
                if (tempString2.equals(tempString)) {
                    Pair pair = new Pair();
                    pair.first = temp.get(0);
                    pair.second = temp.get(i);
                    result.add(pair);
                    temp.remove(i);
                    temp.remove(0);
                    break;
                }
            }
            if (tempSize == temp.size()) temp.remove(0);
        }
    }

    public static class Pair {
        String first;
        String second;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pair pair = (Pair) o;

            if (first != null ? !first.equals(pair.first) : pair.first != null) return false;
            return second != null ? second.equals(pair.second) : pair.second == null;

        }

        @Override
        public int hashCode() {
            int result = first != null ? first.hashCode() : 0;
            result = 31 * result + (second != null ? second.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return  first == null && second == null ? "" :
                        first == null ? second :
                            second == null ? first :
                                first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}
