package com.javarush.task.task08.task0802;

/* 
HashMap из 10 пар
*/

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public static void main(String[] args) throws Exception {
        HashMap<String, String> map = new HashMap<>();
        map.put("арбуз", "ягода");
        map.put("банан", "трава");
        map.put("вишня", "ягода");//напишите тут ваш код
        map.put("груша", "фрукт");//напишите тут ваш код
        map.put("дыня", "овощ");//напишите тут ваш код
        map.put("ежевика", "куст");//напишите тут ваш код
        map.put("жень-шень", "корень");//напишите тут ваш код
        map.put("земляника", "ягода");//напишите тут ваш код
        map.put("ирис", "цветок");//напишите тут ваш код
        map.put("картофель", "клубень");

        for(Map.Entry<String, String> pair: map.entrySet()){
            String key = pair.getKey();
            String value = pair.getValue();
            System.out.println(key + " - " + value);//напишите тут ваш код
        }

    }
}
