package com.javarush.task.task08.task0818;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* 
Только для богачей
*/

public class Solution {
    public static HashMap<String, Integer> createMap() {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Vakhrushev", 510);
        map.put("Tanaev", 510);
        map.put("Flek", 510);
        map.put("Ustinov", 510);
        map.put("Krasnoperov", 510);
        map.put("Vakhrusheva", 450);
        map.put("Mezrin", 520);
        map.put("Mokrov", 520);
        map.put("Pasinkov", 520);
        map.put("Plotnikov", 300);

        return map;
    }

    public static void removeItemFromMap(HashMap<String, Integer> map) {
        Iterator<Map.Entry<String, Integer>> iterator = map.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<String, Integer> pair = iterator.next();
            int v = pair.getValue().intValue();
            if(v < 500) iterator.remove();

        }
    }

    public static void main(String[] args) {

    }
}