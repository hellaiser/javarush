package com.javarush.task.task08.task0828;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* 
Номер месяца
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        HashMap<String, String> month = new HashMap<>();
        month.put("January", "January is the 1 month");//напишите тут ваш код
        month.put("February", "February is the 2 month");//напишите тут ваш код
        month.put("March", "March is the 3 month");//напишите тут ваш код
        month.put("April", "April is the 4 month");//напишите тут ваш код
        month.put("May", "May is the 5 month");//напишите тут ваш код
        month.put("June", "June is the 6 month");//напишите тут ваш код
        month.put("July", "July is the 7 month");//напишите тут ваш код
        month.put("August", "August is the 8 month");//напишите тут ваш код
        month.put("September", "September is the 9 month");//напишите тут ваш код
        month.put("October", "October is the 10 month");//напишите тут ваш код
        month.put("November", "November is the 11 month");//напишите тут ваш код
        month.put("December", "December is the 12 month");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String m = reader.readLine();
        for(Map.Entry<String, String> pair: month.entrySet()){
            if(m.equalsIgnoreCase(pair.getKey())) System.out.println(pair.getValue());
        }
    }
}
