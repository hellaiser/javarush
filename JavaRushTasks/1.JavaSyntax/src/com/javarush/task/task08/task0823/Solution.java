package com.javarush.task.task08.task0823;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Омовение Рамы
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        char[] ch = new char[s.length()];

        for(int i = 0; i < ch.length; i++){
            ch[i] = s.charAt(i);//напишите тут ваш код
        }

        ch[0] = Character.toUpperCase(ch[0]);

        for(int i = 1; i < ch.length; i++){
            if(ch[i - 1] == ' ') ch[i] = Character.toUpperCase(ch[i]);
        }

        System.out.println(s.copyValueOf(ch));


    }
}
