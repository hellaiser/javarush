package com.javarush.task.task08.task0813;

import java.util.Set;
import java.util.HashSet;

/* 
20 слов на букву «Л»
*/

public class Solution {
    public static Set<String> createSet() {
        Set<String> list = new HashSet<>();
        list.add("Лего");
        list.add("Лучина");
        list.add("Лук");
        list.add("Лурк");
        list.add("Лекция");
        list.add("Лектор");
        list.add("Лекторат");
        list.add("Лелик");
        list.add("Лес");
        list.add("Лист");
        list.add("Листва");
        list.add("Лесок");
        list.add("Листок");
        list.add("Лиса");
        list.add("Лисица");
        list.add("Лесной");
        list.add("Лесополоса");
        list.add("Лето");
        list.add("Летник");
        list.add("Лесоруб");
        return list; //напишите тут ваш код

    }

    public static void main(String[] args) {

    }
}
