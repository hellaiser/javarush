package com.javarush.task.task08.task0824;

/* 
Собираем семейство
*/

import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {
        ArrayList<Human> childrenNull = new ArrayList<>();
        Human son = new Human("Artem", true, 32, childrenNull);
        Human daughter = new Human("Natasha", false, 42, childrenNull);
        Human daughter1 = new Human("Kristina", false, 25, childrenNull);

        ArrayList<Human> children = new ArrayList<Human>();
        children.add(son);
        children.add(daughter);
        children.add(daughter1);

        Human father = new Human("Gennadiy", true, 67, children);
        Human mother = new Human("Valentina", false, 64, children);

        ArrayList<Human> children1 = new ArrayList<>();
        children1.add(father);

        ArrayList<Human> children2 = new ArrayList<>();
        children2.add(mother);

        Human grandfatherF = new Human("Dmitriy", true, 98, children1);
        Human grandmotherF = new Human("Lukeriya", false, 95, children1);

        Human grandfatherM = new Human("Petr", true, 88, children2);
        Human grandmotherM = new Human("Lida", false, 85, children2);

        System.out.println(son);
        System.out.println(daughter);
        System.out.println(daughter1);
        System.out.println(father);
        System.out.println(mother);
        System.out.println(grandfatherF);
        System.out.println(grandmotherF);
        System.out.println(grandfatherM);
        System.out.println(grandmotherM);

    }

    public static class Human {
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children;

        public Human(String name, boolean sex, int age, ArrayList<Human> children){
         this.name = name;
         this.sex = sex;
         this.age = age;
         this.children = children;
        }//напишите тут ваш код

        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0) {
                text += ", дети: " + this.children.get(0).name;

                for (int i = 1; i < childCount; i++) {
                    Human child = this.children.get(i);
                    text += ", " + child.name;
                }
            }
            return text;
        }
    }

}
