package com.javarush.task.task08.task0815;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* 
Перепись населения
*/

public class Solution {
    public static HashMap<String, String> createMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("Vakhrushev", "Artem");//напишите тут ваш код
        map.put("Tanaev", "Akeksandr");
        map.put("Flek", "Andrey");
        map.put("Ustinov", "Ruslan");
        map.put("Krasnoperov", "Vasya");
        map.put("Roshin", "Igor");
        map.put("Grehov", "Andrey");
        map.put("Mezrin", "Dima");
        map.put("Mokrov", "Aleksey");
        map.put("Fotina", "Kseniya");
        return map;

    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name) {
        int count = 0;
        for(Map.Entry<String, String> pair : map.entrySet()){
            String value = pair.getValue();
            if(value.equals(name)) count++;//напишите тут ваш код
        }
        return count;
    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String lastName) {
        int count = 0;
        for(Map.Entry<String, String> pair : map.entrySet()){
            String key = pair.getKey();
            if(key.equals(lastName)) count++;//напишите тут ваш код
        }
        return count;
    }

    public static void main(String[] args) {

    }
}
