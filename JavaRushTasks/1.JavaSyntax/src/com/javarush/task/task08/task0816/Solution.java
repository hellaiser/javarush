package com.javarush.task.task08.task0816;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
Добрая Зинаида и летние каникулы
*/

public class Solution {
    public static HashMap<String, Date> createMap() throws ParseException {
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Stallone", new Date(1987, 6, 21));
        map.put("Tanaev", new Date(1987, 6, 21));
        map.put("Vakhrushev", new Date(1987, 6, 21));
        map.put("Fotina", new Date(1991, 1, 20));
        map.put("Vakhrusheva", new Date(1990, 10, 6));
        map.put("VakhrushevaN", new Date(1977, 10, 10));
        map.put("VakhrushevG", new Date(1952, 10, 11));
        map.put("VakhrushevaV", new Date(1955, 6, 14));
        map.put("FotinaE", new Date(1955, 5, 10));
        map.put("Fotin", new Date(1955, 10, 20));

        return map;//напишите тут ваш код
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map) {
        Iterator<Map.Entry<String, Date>> iterator = map.entrySet().iterator();

        while(iterator.hasNext()){
            Map.Entry<String, Date> pair = iterator.next();
            int month = pair.getValue().getMonth();
            if(month > 4 && month < 8) iterator.remove();
        }

    }

    public static void main(String[] args) {

    }
}
