package com.javarush.task.task08.task0827;

import java.util.Date;

/* 
Работа с датой
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(isDateOdd("MAY 1 2013"));
    }

    public static boolean isDateOdd(String date) {
        Date finDate = new Date(date);
        Date startDate = new Date();
        startDate.setYear(finDate.getYear());
        startDate.setMonth(0);
        startDate.setDate(1);
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);

        long msTimeDistance = finDate.getTime() - startDate.getTime();
        long msDay = 24 * 60 * 60 * 1000;

        int dayCount = (int) (msTimeDistance / msDay);
        if(dayCount%2 != 0 || (finDate.getDate() == 1 && finDate.getMonth() == 0)) return true;
        else return false;
    }
}
