package com.javarush.task.task08.task0821;

import java.util.HashMap;
import java.util.Map;

/* 
Однофамильцы и тёзки
*/

public class Solution {
    public static void main(String[] args) {
        Map<String, String> map = createPeopleList();
        printPeopleList(map);
    }

    public static Map<String, String> createPeopleList() {
        HashMap<String, String> map = new HashMap<>();
        map.put("Михайлов", "Павел");
        map.put("Вахрушев", "Артем");
        map.put("Грехов", "Андрей");
        map.put("Курылев", "Андрей");
        map.put("Флек", "Андрей");
        map.put("Танаев", "Александр");
        map.put("Красноперов", "Василий");
        map.put("Пасынков", "Максим");
        map.put("Мокров", "Алексей");
        map.put("Мокров", "Дмитрий");

        return map;
    }

    public static void printPeopleList(Map<String, String> map) {
        for (Map.Entry<String, String> s : map.entrySet()) {
            System.out.println(s.getKey() + " " + s.getValue());
        }
    }
}
