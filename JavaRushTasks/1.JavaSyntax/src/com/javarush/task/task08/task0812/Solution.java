package com.javarush.task.task08.task0812;

import java.io.*;
import java.util.ArrayList;

/* 
Cамая длинная последовательность
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Integer> list = new ArrayList<>();
        int count = 1;
        ArrayList<Integer> list1 = new ArrayList<>();

        for(int i = 0; i < 10; i++){
            list.add(Integer.parseInt(reader.readLine()));//напишите тут ваш код
        }

        for(int i = 0; i < list.size() - 1; i++){
            if(list.get(i).intValue() == list.get(i + 1).intValue()) {
                count ++;
            }
            else {
                list1.add(count);
                count = 1;
            }
        }

        list1.add(count);


        int max = list1.get(0).intValue();
        for(int i = 1; i < list1.size(); i++){
            if(max < list1.get(i).intValue()) max = list1.get(i).intValue();
        }


        System.out.println(max);

    }
}