package com.javarush.task.task08.task0814;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* 
Больше 10? Вы нам не подходите
*/

public class Solution {
    public static HashSet<Integer> createSet() {
        HashSet<Integer> list = new HashSet<>();
        list.add(12);// напишите тут ваш код
        list.add(4);// напишите тут ваш код
        list.add(7);// напишите тут ваш код
        list.add(9);// напишите тут ваш код
        list.add(90);// напишите тут ваш код
        list.add(22);// напишите тут ваш код
        list.add(1);// напишите тут ваш код
        list.add(3);// напишите тут ваш код
        list.add(5);// напишите тут ваш код
        list.add(87);// напишите тут ваш код
        list.add(23);// напишите тут ваш код
        list.add(2);// напишите тут ваш код
        list.add(6);// напишите тут ваш код
        list.add(10);// напишите тут ваш код
        list.add(35);// напишите тут ваш код
        list.add(16);// напишите тут ваш код
        list.add(17);// напишите тут ваш код
        list.add(18);// напишите тут ваш код
        list.add(19);// напишите тут ваш код
        list.add(20);
        return list;// напишите тут ваш код

    }

    public static HashSet<Integer> removeAllNumbersGreaterThan10(HashSet<Integer> set) {
        Iterator<Integer> iterator = set.iterator();
        while(iterator.hasNext()){
            int i = iterator.next();
            if(i > 10) iterator.remove();// напишите тут ваш код
        }

        return set;
    }

    public static void main(String[] args) {

    }
}
