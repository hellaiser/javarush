package com.javarush.task.task09.task0919;

/* 
Деление на ноль
*/

import javax.sound.midi.SysexMessage;

public class Solution {

    public static void main(String[] args) {
        try {
            divideByZero();
        }

        catch (Exception exc){
            exc.printStackTrace();
        }
    }

    public static void divideByZero() throws Exception{
        System.out.println(21/0);
    }
}
