package com.javarush.task.task09.task0927;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* 
Десять котов
*/

public class Solution {
    public static void main(String[] args) {
        Map<String, Cat> map = createMap();
        Set<Cat> set = convertMapToSet(map);
        printCatSet(set);
    }

    public static Map<String, Cat> createMap() {
        Map<String, Cat> map = new HashMap<>();
        map.put("Vasya", new Cat("Vasya"));//напишите тут ваш код
        map.put("Bonya", new Cat("Bonya"));//напишите тут ваш код
        map.put("Ejik", new Cat("Ejik"));//напишите тут ваш код
        map.put("Mishka", new Cat("Mishka"));//напишите тут ваш код
        map.put("Barsik", new Cat("Barsik"));//напишите тут ваш код
        map.put("Hodor", new Cat("Hodor"));//напишите тут ваш код
        map.put("Queen", new Cat("Queen"));//напишите тут ваш код
        map.put("Timofey", new Cat("Timofey"));//напишите тут ваш код
        map.put("Kroha", new Cat("Kroha"));//напишите тут ваш код
        map.put("Melkiy", new Cat("Melkiy"));

        return map;//напишите тут ваш код
    }

    public static Set<Cat> convertMapToSet(Map<String, Cat> map) {
        Set<Cat> set = new HashSet<>();
        for(Map.Entry<String, Cat> pair : map.entrySet()){
            set.add(pair.getValue());//напишите тут ваш код
        }
        return set;
    }

    public static void printCatSet(Set<Cat> set) {
        for (Cat cat : set) {
            System.out.println(cat);
        }
    }

    public static class Cat {
        private String name;

        public Cat(String name) {
            this.name = name;
        }

        public String toString() {
            return "Cat " + this.name;
        }
    }


}
