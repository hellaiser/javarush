package com.javarush.task.task09.task0922;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* 
Какое сегодня число?
*/

public class Solution {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = inFormat.parse(reader.readLine());
        SimpleDateFormat newFormat = new SimpleDateFormat("MMM d, yyy", Locale.ENGLISH);

        System.out.println(newFormat.format(date).toUpperCase());//напишите тут ваш код
    }
}
