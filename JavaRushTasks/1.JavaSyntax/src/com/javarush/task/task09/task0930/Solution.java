package com.javarush.task.task09.task0930;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

/* 
Задача по алгоритмам
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<>();
        while (true) {
            String s = reader.readLine();
            if (s.isEmpty()) break;
            list.add(s);
        }

        String[] array = list.toArray(new String[list.size()]);
        sort(array);

        for (String x : array) {
            System.out.println(x);
        }
    }

    public static void sort(String[] array) {
        ArrayList<String> listS = new ArrayList<>();
        ArrayList<Integer> listI = new ArrayList<>();
        int temp;

        for(int i = 0; i < array.length; i++) {
            if (isNumber(array[i])) listI.add(Integer.parseInt(array[i]));
            else listS.add(array[i]);
        }

        for(int i = 0; i < listI.size(); i++){
            for(int j = (listI.size() - 1); j > i; j--){
                if(listI.get(j) > listI.get(j-1)){
                    temp = listI.get(j);
                    listI.set(j, listI.get(j-1));
                    listI.set(j-1, temp);

                        // напишите тут ваш код
                }
            }
        }

        String[] tempS = new String[listS.size()];
        for(int i = 0; i < tempS.length; i++){
            tempS[i] = listS.get(i);
        }

        Arrays.sort(tempS);
        isGreaterThan(" ", " ");



        for(int i = 0; i < listI.size(); i++){
            array[i] = listI.get(i).toString();
        }

        for(int i = listI.size(), j = 0; i < array.length; i++, j++){
            array[i] = tempS[j];
        }




    }

    // Метод для сравнения строк: 'а' больше чем 'b'
    public static boolean isGreaterThan(String a, String b) {
        return a.compareTo(b) > 0;
    }


    // Переданная строка - это число?
    public static boolean isNumber(String s) {
        if (s.length() == 0) return false;

        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if ((i != 0 && c == '-') // Строка содержит '-'
                    || (!Character.isDigit(c) && c != '-') // или не цифра и не начинается с '-'
                    || (chars.length == 1 && c == '-')) // или одиночный '-'
            {
                return false;
            }
        }
        return true;
    }
}
