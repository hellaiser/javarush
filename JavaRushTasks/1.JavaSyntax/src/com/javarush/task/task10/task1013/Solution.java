package com.javarush.task.task10.task1013;

/* 
Конструкторы класса Human
*/

public class Solution {
    public static void main(String[] args) {
    }

    public static class Human {
        private String name, lastName, address;
        private int age;
        private boolean sex, work;

        public Human(String name, String lastName, boolean sex){ // 1
            this.name = name;
            this.lastName = lastName;
            this.sex = sex;
        }

        public Human(String name, String lastName, int age, boolean sex){ //2
            this.name = name;
            this.lastName = lastName;
            this.age = age;
            this.sex = sex;
        }

        public Human(String name, int age, boolean sex, boolean work){ // 3
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.work = work;
        }

        public Human(String name, int age, boolean sex, String address){ //4
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.address = address;
        }

        public Human(String name, String lastName, int age, String address, boolean sex, boolean work){
            this.name = name;
            this.lastName = lastName;
            this.age = age;
            this.address = address;
            this.sex = sex;
            this.work = work;
        }

        public Human(String name, String lastName, String address){ //6
            this.name = name;
            this.lastName = lastName;
            this.address = address;
        }

        public Human(String name, int age, boolean sex){ //7
            this.name = name;
            this.age = age;
            this.sex = sex;
        }

        public Human(String name, boolean work, int age){ //8
            this.name = name;
            this.work = work;
            this.age = age;
        }

        public Human(){

        }

        public Human(Human human){
            this.name = human.name;
            this.lastName = human.lastName;
            this.age = human.age;
            this.address = human.address;
            this.sex = human.sex;
            this.work = human.work;
        }


        // Напишите тут ваши переменные и конструкторы
    }
}
