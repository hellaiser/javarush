package com.javarush.task.task07.task0703;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Общение одиноких массивов
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        String[] arrayS = new String[10];
        int[] arrayI = new int[10];
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for(int i = 0; i < arrayS.length; i++){
            arrayS[i] = reader.readLine();
            arrayI[i] = arrayS[i].length();
        }
        for(int i = 0; i < arrayI.length; i++){
            System.out.println(arrayI[i]);
        }
        //напишите тут ваш код
    }
}
