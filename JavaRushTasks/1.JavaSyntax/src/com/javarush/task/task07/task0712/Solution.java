package com.javarush.task.task07.task0712;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Самые-самые
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<>();
        int indMin = 0, indMax = 0;

        for(int i = 0; i < 10; i++){
            list.add(reader.readLine());//напишите тут ваш код
        }

        int min = list.get(0).length();

        for(int i = 1; i < list.size(); i++){
            if(min > list.get(i).length()) {
                min = list.get(i).length();
                indMin = i;
            }
        }

        int max = list.get(0).length();

        for(int i = 1; i < list.size(); i++){
            if(max < list.get(i).length()) {
                max = list.get(i).length();
                indMax = i;
            }
        }

        if(indMin < indMax) System.out.println(list.get(indMin));
        else System.out.println(list.get(indMax));
    }
}
