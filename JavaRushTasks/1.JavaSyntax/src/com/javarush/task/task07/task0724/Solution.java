package com.javarush.task.task07.task0724;

/* 
Семейная перепись
*/

public class Solution {
    public static void main(String[] args) {
        Human anya = new Human("Anne", false, 22);
        Human tanya = new Human("Tanya", false,21);
        Human maxim = new Human("Maxim", true, 24);
        Human andrey = new Human("Andrey", true, 20);


        Human alisa = new Human("Alisa", false, 25, new Human ("Andrey"), new Human ("Vera"));


        Human vika = new Human("Vika", false, 24, new Human ("Andrey"), new Human ("Vera"));


        Human aleksndr = new Human("Aleksander", true, 34, new Human ("Andrey"), new Human ("Vera"));


        Human dima = new Human("Dima", true, 25, new Human ("Andrey"), new Human ("Vera"));


        Human sveta = new Human("Sveta", false, 23, new Human ("Andrey"), new Human ("Vera"));// напишите тут ваш код

        System.out.println(anya);
        System.out.println(tanya);
        System.out.println(maxim);
        System.out.println(andrey);
        System.out.println(alisa);
        System.out.println(vika);
        System.out.println(aleksndr);
        System.out.println(dima);
        System.out.println(sveta);
    }



    public static class Human {
        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        public Human(String name){
            this.name = name;
        }

        public Human(String name, boolean sex, int age){
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public Human(String name, boolean sex, int age, Human father, Human mother){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }// напишите тут ваш код

        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }
}