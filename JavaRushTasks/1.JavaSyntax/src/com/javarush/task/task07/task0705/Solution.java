package com.javarush.task.task07.task0705;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Один большой массив и два маленьких
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        int[] arrayB = new int[20];
        int[] arrayS1 = new int[10];
        int[] arrayS2 = new int[10];
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for(int i = 0; i < arrayB.length; i++){
            arrayB[i] = Integer.parseInt(reader.readLine());
        }

        for(int i = 0, j = 10; i < 10; i++, j++){
            arrayS1[i] = arrayB[i];
            arrayS2[i] = arrayB[j];
        }

        for(int i = 0; i < arrayS2.length; i++){
            System.out.println(arrayS2[i]);
        }
    }
}
