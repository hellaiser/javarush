package com.javarush.task.task07.task0706;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Улицы и дома
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] array = new int[15];
        int even = 0, odd = 0;

        for(int i = 0; i < array.length; i++){
            array[i] = Integer.parseInt(reader.readLine());//напишите тут ваш код
        }

        for(int i = 0, j = 1; i < array.length && j < array.length; i+=2, j+=2){
            even += array[i];
            odd += array[j];
        }

        if(even > odd) System.out.println("В домах с четными номерами проживает больше жителей.");
        else System.out.println("В домах с нечетными номерами проживает больше жителей.");
    }
}
