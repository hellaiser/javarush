package com.javarush.task.task20.task2002;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* 
Читаем и пишем в файл: JavaRush
*/
public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or adjust outputStream/inputStream according to your file's actual location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {
            File yourFile = File.createTempFile("d:\\test.txt", null);
            OutputStream outputStream = new FileOutputStream(yourFile);
            InputStream inputStream = new FileInputStream(yourFile);

            JavaRush javaRush = new JavaRush();
            User user1 = new User();
            user1.setFirstName("Artem");
            user1.setLastName("Vahrushev");
            user1.setMale(true);
            user1.setBirthDate(new Date(1987, 2, 30));
            user1.setCountry(User.Country.RUSSIA);
            javaRush.users.add(user1);
            User user2 = new User();
            user2.setFirstName("Nastya");
            user2.setLastName("Vahrusheva");
            user2.setBirthDate(new Date(1990, 10, 6));
            user2.setCountry(User.Country.RUSSIA);
            javaRush.users.add(user2);//initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
            javaRush.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);
            //here check that the javaRush object is equal to the loadedObject object - проверьте тут, что javaRush и loadedObject равны

            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something is wrong with my file");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Oops, something is wrong with the save/load method");
        }
    }

    public static class JavaRush {
        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception {
            String s = null;
            String isUsersPreset = users.size() > 0 ? "yes" : "no";
            outputStream.write(isUsersPreset.getBytes());
            outputStream.write('\n');

            if (users.size() > 0) {
                for (int i = 0; i < users.size(); i++) {
                    String firstname = users.get(i).getFirstName();
                    if (firstname != null) {
                        outputStream.write("yes".getBytes());
                        outputStream.write(' ');
                        outputStream.write(firstname.getBytes());
                        outputStream.write('\n');
                    }
                    else {
                        outputStream.write("no".getBytes());
                        outputStream.write('\n');
                    }


                    String lastName = users.get(i).getLastName();
                    if (lastName != null) {
                        outputStream.write("yes".getBytes());
                        outputStream.write(' ');
                        outputStream.write(lastName.getBytes());
                        outputStream.write('\n');
                    }
                    else {
                        outputStream.write("no".getBytes());
                        outputStream.write('\n');
                    }

                    long date = users.get(i).getBirthDate().getTime();
                    if (date != 0) {
                        outputStream.write("yes".getBytes());
                        outputStream.write(' ');
                        outputStream.write(s.valueOf(date).getBytes());
                        outputStream.write('\n');//implement this method - реализуйте этот метод
                    }
                    else {
                        outputStream.write("no".getBytes());
                        outputStream.write('\n');
                    }

                    boolean male = users.get(i).isMale();
                    if (male) {
                        outputStream.write("male".getBytes());
                        outputStream.write('\n');
                    }
                    else {
                        outputStream.write("female".getBytes());
                        outputStream.write('\n');
                    }

                    String country = users.get(i).getCountry().toString();
                    if (country != null) {
                        outputStream.write("yes".getBytes());
                        outputStream.write(' ');
                        outputStream.write(country.getBytes());
                        outputStream.write('\n');
                    }
                    else {
                        outputStream.write("no".getBytes());
                        outputStream.write('\n');
                    }
                }
            }
        }

        public void load(InputStream inputStream) throws Exception {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            if (reader.readLine().equals("yes")) {
                while (true) {
                    User user = new User();
                    String s = reader.readLine();
                    if (s == null) break;
                    if (s.startsWith("yes")) {
                        user.setFirstName(s.split(" ")[1]);//implement this method - реализуйте этот метод
                    }

                    s = reader.readLine();
                    if (s.startsWith("yes")) {
                        user.setLastName(s.split(" ")[1]);
                    }

                    s = reader.readLine();
                    if (s.startsWith("yes")) {
                        long date = Long.parseLong(s.split(" ")[1]);
                        user.setBirthDate(new Date(date));
                    }

                    s = reader.readLine();
                    if (s.equals("male")) user.setMale(true);
                    else user.setMale(false);

                    s = reader.readLine();
                    if (s.startsWith("yes")) {
                        String country = s.split(" ")[1];
                        country = country.toLowerCase();
                        if (country.equals("ukraine")) user.setCountry(User.Country.UKRAINE);
                        if (country.equals("russia")) user.setCountry(User.Country.RUSSIA);
                        if (country.equals("other")) user.setCountry(User.Country.OTHER);
                    }

                    users.add(user);
                }
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            JavaRush javaRush = (JavaRush) o;

            return users != null ? users.equals(javaRush.users) : javaRush.users == null;

        }

        @Override
        public int hashCode() {
            return users != null ? users.hashCode() : 0;
        }
    }
}
