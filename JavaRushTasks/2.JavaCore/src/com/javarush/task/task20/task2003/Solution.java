package com.javarush.task.task20.task2003;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/* 
Знакомство с properties
*/
public class Solution {
    public static Map<String, String> properties = new HashMap<>();

    public void fillInPropertiesMap() throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        FileInputStream fileInputStream = new FileInputStream(fileName);
        load(fileInputStream);
        //implement this method - реализуйте этот метод
    }

    public void save(OutputStream outputStream) throws Exception {
        Properties properties1 = new Properties();
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            properties1.setProperty(entry.getKey(), entry.getValue());//implement this method - реализуйте этот метод
        }
        properties1.store(outputStream, null);
    }

    public void load(InputStream inputStream) throws Exception {
        Properties properties1 = new Properties();
        properties1.load(inputStream);
        for (String name : properties1.stringPropertyNames()) {
            properties.put(name, properties1.getProperty(name));
        }
    }

    public static void main(String[] args) {

    }
}
