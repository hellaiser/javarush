package com.javarush.task.task14.task1419;

import java.io.IOException;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.NoSuchElementException;

/* 
Нашествие исключений
*/

public class Solution {
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args) {
        initExceptions();

        for (Exception exception : exceptions) {
            System.out.println(exception);
        }
    }

    private static void initExceptions() {   //the first exception
        try {
            float i = 1 / 0;

        } catch (Exception e) {
            exceptions.add(e);
        }

        try { //2
            int[] arrays = new int[1];
            int i = arrays[2];//напишите тут ваш код
        }

        catch (ArrayIndexOutOfBoundsException exc){
            exceptions.add(exc);
        }

        try { //3
            int[] arrays = new int[-2];
        }

        catch (NegativeArraySizeException exc){
            exceptions.add(exc);
        }

        try{ //4
            throw new NullPointerException();
        }

        catch (NullPointerException exc){
            exceptions.add(exc);
        }

        try{//5
            throw new NumberFormatException();
        }

        catch (NumberFormatException exc){
            exceptions.add(exc);
        }

        try{//6
            throw new SecurityException();
        }

        catch (SecurityException exc){
            exceptions.add(exc);
        }

        try{//7
            throw new StringIndexOutOfBoundsException();
        }

        catch (StringIndexOutOfBoundsException exc){
            exceptions.add(exc);
        }

        try { //8
            throw new NoSuchElementException();
        }

        catch (NoSuchElementException exc){
            exceptions.add(exc);
        }

        try { //9
            throw new IllegalArgumentException();
        }

        catch (IllegalArgumentException exc){
            exceptions.add(exc);
        }

        try {//10
            throw new EmptyStackException();
        }

        catch (EmptyStackException exc){
            exceptions.add(exc);
        }



    }
}
