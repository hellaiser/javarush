package com.javarush.task.task14.task1420;

/* 
НОД
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String s = reader.readLine();
        int i = Integer.parseInt(s);

        String m = reader.readLine();
        int j = Integer.parseInt(m);
        if (i <= 0 || j <= 0) throw new Exception();






        int nod = 0;

        if(i < j){
            for(int x = 0; x <= i; x++){
                if((i % x == 0) && (j % x == 0)) nod = x;
            }
        }
        else {
            for(int x = 0; x <= j; x++){
                if((i % x == 0) && (j % x == 0)) nod = x;
            }
        }

        System.out.println(nod);
    }
}
