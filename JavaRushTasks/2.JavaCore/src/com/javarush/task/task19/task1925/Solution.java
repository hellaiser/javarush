package com.javarush.task.task19.task1925;

/* 
Длинные слова
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(args[0]));
        FileWriter fileWriter = new FileWriter(args[1]);

        String s;
        ArrayList<String> list = new ArrayList<>();
        while ((s = fileReader.readLine()) != null) {
            String[] array = s.split(" ");
            for (int i = 0; i < array.length; i++) {
                if (array[i].length() > 6) list.add(array[i]);
            }
        }
        fileReader.close();

        for (int i = 0; i < list.size() - 1; i++) {
            fileWriter.write(list.get(i) + ",");
        }

        fileWriter.write(list.get(list.size() - 1));
        fileWriter.close();
    }
}
