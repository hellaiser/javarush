package com.javarush.task.task19.task1908;

/* 
Выделяем числа
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        String fileOutName = reader.readLine();
        reader.close();

        BufferedReader  fileReader = new BufferedReader(new FileReader(fileName));
        BufferedWriter fileOut = new BufferedWriter(new FileWriter(fileOutName, true));

        String s;
        while ((s = fileReader.readLine()) != null) {
            String[] buffer = s.split(" ");
            for (int i = 0; i < buffer.length; i++) {
                try {
                    String write = Integer.parseInt(buffer[i]) + " ";

                    fileOut.write(write);
                }

                catch (NumberFormatException exc){}
            }
        }

        fileReader.close();
        fileOut.close();
    }
}
