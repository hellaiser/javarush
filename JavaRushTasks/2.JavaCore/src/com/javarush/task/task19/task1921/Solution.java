package com.javarush.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* 
Хуан Хуанович
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(args[0]));
        String s;

        while ((s = fileReader.readLine()) != null) {
            String[] array = s.split(" ");
            int length = array.length;
            SimpleDateFormat nDate = new SimpleDateFormat("dd MM YYYY");
            String sBirthday = array[array.length - 3] + " " + array[array.length - 2] + " " + array[array.length - 1];
            Date birthdate = null;
            try {
                birthdate = new SimpleDateFormat("dd MM yyyy").parse(sBirthday);
            }
            catch (ParseException exc) {}
            String name = array[0];
            for (int i = 1; i <= (length - 4); i++) {
                name = name + " " + array[i];
            }
            PEOPLE.add(new Person(name, birthdate));

        }

        fileReader.close();

    }
}
