package com.javarush.task.task19.task1927;

/* 
Контекстная реклама
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream consoleStream = System.out;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        System.setOut(printStream);

        testString.printSomething();

        System.setOut(consoleStream);

        String s = outputStream.toString();
        String[] array = s.split("\\n");

        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (count == 2) {
                System.out.println("JavaRush - курсы Java онлайн");
                count = 0;
                i--;
            }
            else {
                System.out.println(array[i]);
                count ++;
            }
        }
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("first");
            System.out.println("second");
            System.out.println("third");
            System.out.println("fourth");
            System.out.println("fifth");
        }
    }
}
