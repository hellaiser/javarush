package com.javarush.task.task19.task1920;

/* 
Самый богатый
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(args[0]));
        TreeMap<String, Double> map = new TreeMap<>();
        String s;

        while ((s = fileReader.readLine()) != null) {
            String[] array = s.split(" ");
            if (map.containsKey(array[0])) map.put(array[0], map.get(array[0]) + Double.parseDouble(array[1]));
            else map.put(array[0], Double.parseDouble(array[1]));
        }

        fileReader.close();

        double max = map.get(map.firstKey());

        for (Map.Entry<String, Double> entry : map.entrySet()) {
            double temp = entry.getValue();
            if (max < temp) max = temp;
        }

        for (Map.Entry<String, Double> entry : map.entrySet()) {
            if (entry.getValue() == max) System.out.println(entry.getKey());
        }
    }
}
