package com.javarush.task.task19.task1906;

/* 
Четные символы
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileIn = reader.readLine();
        String fileOut = reader.readLine();
        reader.close();

        FileReader fileReader = new FileReader(fileIn);
        FileWriter fileWriter = new FileWriter(fileOut);

        while(fileReader.ready()) {
            int data = fileReader.read();
            data = fileReader.read();
            fileWriter.write(data);
        }

        fileReader.close();
        fileWriter.close();
    }
}
