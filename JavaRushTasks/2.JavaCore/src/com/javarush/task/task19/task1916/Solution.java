package com.javarush.task.task19.task1916;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Отслеживаем изменения
*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader file1 = new BufferedReader(new FileReader(reader.readLine()));
        BufferedReader file2 = new BufferedReader(new FileReader(reader.readLine()));
        reader.close();

        String s;
        ArrayList<String> file1List = new ArrayList<>();
        while((s = file1.readLine()) != null){
            file1List.add(s);
        }

        ArrayList<String> file2List = new ArrayList<>();
        while ((s = file2.readLine()) != null) {
            file2List.add(s);
        }

        file1.close();
        file2.close();

        int i = 0;
        while (i < file1List.size() && i < file2List.size()) {
            if (file1List.get(i).equals(file2List.get(i))) {
                lines.add(new LineItem(Type.SAME, file1List.get(i)));
                file1List.remove(i);
                file2List.remove(i);
            }
            else if (!(file1List.get(i).equals(file2List.get(i))) && file2List.get(i).equals(file1List.get(i + 1))) {
                lines.add(new LineItem(Type.REMOVED, file1List.get(i)));
                file1List.remove(i);
            }

            else if (!(file1List.get(i).equals(file2List.get(i))) && file1List.get(i).equals(file2List.get(i + 1))) {
                lines.add(new LineItem(Type.ADDED, file2List.get(i)));
                file2List.remove(i);
            }
        }

        for(String temp: file1List) {
            lines.add(new LineItem(Type.REMOVED, temp));
        }

        for (String temp : file2List) {
            lines.add(new LineItem(Type.ADDED, temp));
        }
    }


    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
    }
}
