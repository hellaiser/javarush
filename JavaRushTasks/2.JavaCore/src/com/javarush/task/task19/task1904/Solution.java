package com.javarush.task.task19.task1904;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.Date;

/* 
И еще один адаптер
*/

public class Solution {

    public static void main(String[] args) {

    }

    public static class PersonScannerAdapter implements PersonScanner{
        private Scanner fileScanner;

        public PersonScannerAdapter(Scanner fileScanner) {
            this.fileScanner = fileScanner;
        }

        @Override
        public void close() throws IOException {
            fileScanner.close();
        }

        @Override
        public Person read() throws IOException {
            String personScanner = fileScanner.nextLine();
            String firstName = personScanner.split(" ")[1];;
            String lastName = personScanner.split(" ")[0];;
            String middleName = personScanner.split(" ")[2];;
            String sBirthday = personScanner.split(" ")[3] + " " + personScanner.split(" ")[4] +" " + personScanner.split(" ")[5];
            Date birthday = null;
            try {
                birthday = new SimpleDateFormat("dd MM yyyy").parse(sBirthday);
            }

            catch (ParseException exc){}
            return new Person(firstName, middleName, lastName, birthday);
        }
    }
}
