package com.javarush.task.task19.task1918;

/* 
Знакомство с тегами
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader fileReader = new BufferedReader(new FileReader(reader.readLine()));
        reader.close();

        String s, line = null;

        while((s = fileReader.readLine()) != null) {
            line = line + s;
        }

        fileReader.close();

        String openTag = "<" + args[0];
        String closeTag = "</" + args[0] + ">";

        int startIndex = 0, index;

        ArrayList<Integer> openList = new ArrayList<>();
        while(true) {
            index = line.indexOf(openTag, startIndex);
            if (index == -1) break;
            openList.add(index);
            startIndex = ++ index;
        }

        ArrayList<Integer> closeList = new ArrayList<>();
        startIndex = 0;
        while (true) {
            index = line.indexOf(closeTag, startIndex);
            if (index == -1) break;
            closeList.add(index);
            startIndex = ++index;
        }

        int closeId, openId, level = 0;

        while (openList.size() > 0) {
            for (int i = 0; i < openList.size(); i++) {
                if (openList.get(i) < closeList.get(0)) level++;
                else break;
            }

            for (int i = level - 1; i >= 0; i--){
                openId = openList.get(0);
                closeId = closeList.get(i);
                s = line.substring(openId, closeId);
                System.out.println(s + closeTag);
                openList.remove(0);
                closeList.remove(i);
                level = 0;
                break;
            }
        }
    }
}
