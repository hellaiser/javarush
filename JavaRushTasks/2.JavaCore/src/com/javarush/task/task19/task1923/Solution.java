package com.javarush.task.task19.task1923;

/* 
Слова с цифрами
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(args[0]));
        FileWriter fileWriter = new FileWriter(args[1]);

        String s;
        String sum = null;
        while ((s = fileReader.readLine()) != null) {
            String[] array = s.split(" ");
            for (int i = 0; i < array.length; i ++) {
                if (array[i].matches(".*\\d+.*")) {
                    if (sum == null) sum = array[i] + " ";
                    else sum = sum + array[i] + " ";
                }
            }
        }

        fileWriter.write(sum);

        fileReader.close();
        fileWriter.close();
    }
}
