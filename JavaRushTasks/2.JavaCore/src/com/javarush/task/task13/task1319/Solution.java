package com.javarush.task.task13.task1319;

import java.io.*;

/* 
Писатель в файл с консоли
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileWriter outputFile = new FileWriter(reader.readLine());
        BufferedWriter writer = new BufferedWriter(outputFile);

        String s;
        do{
            s = reader.readLine();
            writer.write(s + "\r\n");
        }
        while(!s.equals("exit"));

        writer.close();
    }
}
