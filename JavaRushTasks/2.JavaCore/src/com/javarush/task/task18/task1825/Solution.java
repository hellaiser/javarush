package com.javarush.task.task18.task1825;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/* 
Собираем файл
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        TreeSet<String> sortedFilesNames = new TreeSet<>();
        String s;

        while(true) {
            s = reader.readLine();
            if (s.equals("end")) break;
            sortedFilesNames.add(s);
        }

        s = sortedFilesNames.first();

        String outputFilePath = s.split(".part")[0];
        FileOutputStream fileOutputStream = new FileOutputStream(outputFilePath, true);

        Iterator<String> iterator = sortedFilesNames.iterator();

        while (iterator.hasNext()) {
            FileInputStream fileInputStream = new FileInputStream(iterator.next());
            byte[] buffer = new byte[fileInputStream.available()];
            fileInputStream.read(buffer);
            fileOutputStream.write(buffer);
            fileInputStream.close();
        }

        fileOutputStream.close();
    }
}
