package com.javarush.task.task18.task1810;

/* 
DownloadException
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws DownloadException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fileIn;
        try {
        do {
            fileIn = new FileInputStream(reader.readLine());
        }

        while (fileIn.available() >= 1000);

        fileIn.close();
        throw new DownloadException();
        }

        catch (IOException exc) {}


    }

    public static class DownloadException extends Exception {

    }
}
