package com.javarush.task.task18.task1828;

/* 
Прайсы 2
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        String id, productName, price, quantity;

        if (args.length > 1) {
            BufferedReader fileReader = new BufferedReader(new FileReader(fileName));
            ArrayList<String> priceList = new ArrayList<>();
            String s;
            while ((s = fileReader.readLine()) != null) {
                priceList.add(s);
            }
            fileReader.close();
            if (args[0].equals("-u")) {
                id = args[1];
                if (id.length() < 8) {
                    while (id.length() != 8) {
                        id = id + " ";
                    }
                }

                if (args[2].length() < 30) {
                    productName = args[2];
                    while (productName.length() != 30) {
                        productName = productName + " ";
                    }
                }

                else if (args[2].length() > 30) productName = args[2].substring(0, 31);

                else productName = args[2];

                if (args[3].length() < 8) {
                    price = args[3];
                    while (price.length() != 8) {
                        price = price + " ";
                    }
                }

                else if (args[3].length() > 8) price = args[3].substring(0, 9);

                else  price = args[3];

                if (args[4].length() < 4) {
                    quantity = args[4];
                    while (quantity.length() != 4) {
                        quantity = quantity + " ";
                    }
                }

                else if (args[4].length() > 4) quantity = args[4].substring(0, 5);

                else quantity = args[4];

                for(int i = 0; i < priceList.size(); i++) {
                    if (priceList.get(i).startsWith(args[1])) {
                        priceList.set(i, (id + productName + price + quantity));
                    }
                }

                FileWriter fileWriterClean = new FileWriter(fileName);
                fileWriterClean.close();
                FileWriter fileWriter = new FileWriter(fileName, true);
                fileWriter.write(priceList.get(0));
                for (int i = 1; i < priceList.size(); i ++) {
                    fileWriter.write("\n");
                    fileWriter.write(priceList.get(i));
                }

                fileWriter.close();
            }

            else if (args[0].equals("-d")) {
                for (int i = 0; i < priceList.size(); i ++) {
                    if (priceList.get(i).startsWith(args[1])) {
                        priceList.remove(i);
                    }
                }
                FileWriter fileWriterClean = new FileWriter(fileName);
                fileWriterClean.close();
                FileWriter fileWriter = new FileWriter(fileName, true);
                fileWriter.write(priceList.get(0));
                for (int i = 1; i < priceList.size(); i ++) {
                    fileWriter.write("\n");
                    fileWriter.write(priceList.get(i));
                }

                fileWriter.close();
            }
        }
    }
}
