package com.javarush.task.task18.task1819;

/* 
Объединение файлов
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();

        FileInputStream file1In = new FileInputStream(file1);
        byte[] buffer1 = new byte[file1In.available()];

        int ind = file1In.read(buffer1);

        file1In.close();

        FileOutputStream file1Out = new FileOutputStream(file1);
        FileInputStream file2In = new FileInputStream(file2);

        byte[] buffer2 = new byte[file2In.available()];
        file2In.read(buffer2);

        byte[] generalBuffer = new byte[(buffer1.length + buffer2.length)];

        for (int i = 0; i < buffer2.length; i++) {
            generalBuffer[i] = buffer2[i];
        }

        for (int i = buffer2.length, j =0; i < buffer1.length + buffer2.length; i++, j++) {
            generalBuffer[i] = buffer1[j];
        }

        file1Out.write(generalBuffer);

        file1Out.close();
        file2In.close();
    }
}
