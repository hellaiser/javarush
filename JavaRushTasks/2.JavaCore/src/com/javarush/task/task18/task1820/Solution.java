package com.javarush.task.task18.task1820;

/* 
Округление чисел
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream file1 = new FileInputStream(reader.readLine());
        FileOutputStream file2 = new FileOutputStream(reader.readLine(), true);
        char c;



        byte[] buffer = new byte[file1.available()];

        file1.read(buffer);
        String s = null;


        for(int i = 0; i < buffer.length;) {
            s = s.valueOf((char) buffer[i]);
            i++;
            while (true) {
                if(buffer[i] == 32) {
                    i++;
                    break;
                }
                s += s.valueOf((char) buffer[i]);
                i++;
                if(i == buffer.length) break;
            }

            double d = Double.parseDouble(s);

            int result = (int) Math.round(d);

            s = s.valueOf(result) + " ";

            file2.write(s.getBytes());

        }

        file1.close();
        file2.close();

    }
}
