package com.javarush.task.task18.task1817;

/* 
Пробелы
*/

import java.io.FileInputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        FileInputStream file = new FileInputStream(args[0]);
        byte[] buffer = new byte[file.available()];
        int count = file.read(buffer);
        int countSpaces = 0;

        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] == 32) countSpaces++;
        }

        file.close();

        double result = (double) countSpaces/count * 100;

        System.out.format("%.2f", result);

    }
}
