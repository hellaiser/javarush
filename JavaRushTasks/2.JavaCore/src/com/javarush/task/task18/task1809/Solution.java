package com.javarush.task.task18.task1809;

/* 
Реверс файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fileIn = new FileInputStream(reader.readLine());
        FileOutputStream fileOut = new FileOutputStream(reader.readLine());

        byte[] buffer = new byte[fileIn.available()];
        fileIn.read(buffer);

        for (int i = buffer.length - 1; i >= 0; i--) {
            fileOut.write(buffer[i]);
        }

        fileIn.close();
        fileOut.close();
    }
}
