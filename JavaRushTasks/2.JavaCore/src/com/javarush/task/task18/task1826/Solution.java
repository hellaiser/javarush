package com.javarush.task.task18.task1826;

/* 
Шифровка
*/

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        int key = 11;
        if (args[0].equals("-e")) {
            FileInputStream fileInputStream = new FileInputStream(args[1]);
            FileOutputStream fileOutputStream = new FileOutputStream(args[2], true);
            while (fileInputStream.available() > 0) {
                int readByte = fileInputStream.read();
                fileOutputStream.write((readByte ^ key));
            }

            fileInputStream.close();
            fileOutputStream.close();
        }

        else if (args[0].equals("-d")) {
            FileInputStream fileInputStream = new FileInputStream(args[1]);
            FileOutputStream fileOutputStream = new FileOutputStream(args[2], true);
            while (fileInputStream.available() > 0) {
                int readByte = fileInputStream.read();
                fileOutputStream.write((readByte ^ key));
            }

            fileInputStream.close();
            fileOutputStream.close();
        }
    }

}
