package com.javarush.task.task18.task1818;

/* 
Два в одном
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileOutputStream fileOut = new FileOutputStream(reader.readLine(), true);
        FileInputStream fileIn1 = new FileInputStream(reader.readLine());
        FileInputStream fileIn2 = new FileInputStream(reader.readLine());

        while (fileIn1.available() > 0) {
            fileOut.write(fileIn1.read());
        }

        while (fileIn2.available() > 0) {
            fileOut.write(fileIn2.read());
        }

        fileIn1.close();
        fileIn2.close();
        fileOut.close();
    }
}
