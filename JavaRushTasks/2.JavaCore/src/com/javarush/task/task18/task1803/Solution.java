package com.javarush.task.task18.task1803;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* 
Самые частые байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream file = new FileInputStream(reader.readLine());
        ArrayList<Integer> bytes = new ArrayList<>();
        HashMap <Integer, Integer> countBytes = new HashMap<>();

        while (file.available() > 0) {
            bytes.add(file.read());
        }

        file.close();

        while(!bytes.isEmpty()) {
            int count = 1;
            for (int i = 1; i < bytes.size();) {
                if(bytes.get(0) == bytes.get(i)) {
                    count++;
                    bytes.remove(i);
                }
                else i++;
            }
            //System.out.println(bytes.get(0) + " " + count);
            countBytes.put(bytes.get(0), count);
            bytes.remove(0);
        }

        int max = 0;
        for(Map.Entry<Integer, Integer> pair : countBytes.entrySet()) {
            if(pair.getValue() > max) max = pair.getValue();
        }

        for(Map.Entry<Integer, Integer> pair : countBytes.entrySet()) {
            if(pair.getValue() == max) System.out.print(pair.getKey() + " ");
        }
    }
}
