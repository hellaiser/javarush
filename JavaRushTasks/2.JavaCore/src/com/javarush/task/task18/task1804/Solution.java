package com.javarush.task.task18.task1804;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/* 
Самые редкие байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream file = new FileInputStream(reader.readLine());

        ArrayList<Integer> bytes = new ArrayList<>();
        while (file.available() > 0) {
            bytes.add(file.read());
        }
        file.close();

        HashMap<Integer, Integer> countBytes = new HashMap<>();
        while (!bytes.isEmpty()) {
            int count = 1;
            for (int i = 1; i < bytes.size();) {
                if (bytes.get(0) == bytes.get(i)) {
                    count++;
                    bytes.remove(i);
                }
                else i++;
            }

            countBytes.put(bytes.get(0), count);
            bytes.remove(0);
        }

        ArrayList<Integer> minCountBytes = new ArrayList<>();
        for (Map.Entry<Integer, Integer> pair : countBytes.entrySet()) {
            minCountBytes.add(pair.getValue());
        }

        int min = minCountBytes.get(0);

        for(int i = 1; i < minCountBytes.size(); i++) {
            if (min > minCountBytes.get(i)) min = minCountBytes.get(i);
        }

        for (Map.Entry<Integer, Integer> pair : countBytes.entrySet()) {
            if (pair.getValue() == min) System.out.print(pair.getKey() + " ");
        }


    }
}
