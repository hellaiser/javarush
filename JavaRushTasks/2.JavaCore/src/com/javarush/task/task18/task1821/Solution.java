package com.javarush.task.task18.task1821;

/* 
Встречаемость символов
*/


import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class Solution {
    public static void main(String[] args) throws IOException {
        FileInputStream file = new FileInputStream(args[0]);
        ArrayList<Integer> list = new ArrayList<>();

        while (file.available() > 0){
            list.add(file.read());
        }
        file.close();

        HashMap<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < list.size(); i++) {
            int count = 1;
            for(int j = 1; j < list.size();) {
                if(list.get(i) == list.get(j)) {
                    count++;
                    list.remove(j);
                }
                else j++;
            }
            map.put(list.get(i), count);
        }

        Collections.sort(list);

        for(int i = 0; i < list.size(); i++) {
            for(Map.Entry<Integer, Integer> pair : map.entrySet()) {
                if (list.get(i) == pair.getKey()) {
                    int b = pair.getKey();
                    System.out.println((char) b + " " + pair.getValue());
                }
            }
        }
    }
}
