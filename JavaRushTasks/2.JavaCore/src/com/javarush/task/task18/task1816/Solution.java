package com.javarush.task.task18.task1816;

/* 
Английские буквы
*/

import java.io.FileInputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        FileInputStream file = new FileInputStream(args[0]);
        int count = 0;
        while(file.available() > 0) {
            char c = (char) file.read();
            if((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) count++;
        }

        file.close();

        System.out.println(count);
    }
}
