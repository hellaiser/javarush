package com.javarush.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeSet;

/* 
Сортировка байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream file = new FileInputStream(reader.readLine());

        TreeSet<Integer> bytes = new TreeSet<>();

        while (file.available() > 0) {
            bytes.add(file.read());
        }

        file.close();

        ArrayList<Integer> sortBytes = new ArrayList<>();

        sortBytes.addAll(bytes);

        Collections.sort(sortBytes);

        for (int i : sortBytes) {
            System.out.print(i + " ");
        }
    }
}
