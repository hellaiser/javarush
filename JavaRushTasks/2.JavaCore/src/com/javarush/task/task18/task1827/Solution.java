package com.javarush.task.task18.task1827;

/* 
Прайсы
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.TreeSet;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        String id, productName, price, quantity;
        reader.close();

        if(args.length == 4 && args[0].equals("-c")) {
            BufferedReader fileReader = new BufferedReader(new FileReader(fileName));
            String s;
            TreeSet<Long> sortedId = new TreeSet<>();
            while ((s = fileReader.readLine()) != null) {
                s = s.substring(0, 8).trim();
                sortedId.add(Long.parseLong(s));
            }

            fileReader.close();

            id = s.valueOf((sortedId.last() + 1));

            if (id.length() < 8) {
                while (id.length() != 8) {
                    id = id + " ";
                }
            }


            if (args[1].length() < 30) {
                productName = args[1];
                while (productName.length() != 30) {
                    productName = productName + " ";
                }
            }

            else if (args[1].length() > 30) productName = args[1].substring(0, 31);

            else productName = args[1];

            if (args[2].length() < 8) {
                price = args[2];
                while (price.length() != 8) {
                    price = price + " ";
                }
            }

            else if (args[2].length() > 8) price = args[2].substring(0, 9);

            else  price = args[2];

            if (args[3].length() < 4) {
                quantity = args[3];
                while (quantity.length() != 4) {
                    quantity = quantity + " ";
                }
            }

            else if (args[3].length() > 4) quantity = args[3].substring(0, 5);

            else quantity = args[3];

            FileWriter fileWriter = new FileWriter(fileName, true);
            fileWriter.write("\n");
            fileWriter.write(id + productName + price + quantity);

            fileWriter.close();
        }
    }
}
