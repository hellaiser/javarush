package com.javarush.task.task17.task1721;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Транзакционность
*/

public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();

    public static void main(String[] args) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader file1;
        BufferedReader file2;
        try {
            file1 = new BufferedReader(new FileReader(reader.readLine()));
            file2 = new BufferedReader(new FileReader(reader.readLine()));

            String s1, s2;
            while ((s1 = file1.readLine()) != null) {
                allLines.add(s1);
            }

            while ((s2 = file2.readLine()) != null) {
                forRemoveLines.add(s2);
            }
            reader.close();
            file1.close();
            file2.close();
        }

        catch (IOException exc){}

        try {
            new Solution().joinData();
        }

        catch (CorruptedDataException exc) {}

    }

    public void joinData() throws CorruptedDataException {
        if(allLines.containsAll(forRemoveLines)) allLines.removeAll(forRemoveLines);
        else {
            allLines.clear();
            throw new CorruptedDataException();
        }





    }
}
