package com.javarush.task.task17.task1710;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) {
        SimpleDateFormat date = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        SimpleDateFormat nDate = new SimpleDateFormat("dd/MM/yyyy");
        switch (args[0]) {
            case "-c" : {
                try {
                    if (args[2].contains("м")) allPeople.add(Person.createMale(args[1], nDate.parse(args[3])));
                    else allPeople.add(Person.createFemale(args[1], nDate.parse(args[3])));
                    System.out.println((allPeople.size() - 1));
                } catch (ParseException exc) {
                }
            }
                break;

            case "-u" : {
                Person person = allPeople.get(Integer.parseInt(args[1]));
                person.setName(args[2]);
                if(args[3].contains("м")) person.setSex(Sex.MALE);
                else person.setSex(Sex.FEMALE);
                try {
                    person.setBirthDate(nDate.parse(args[4]));//start here - начни тут
                }

                catch (ParseException exc) {}


            }

            break;

            case "-d" : {
                Person person = allPeople.get(Integer.parseInt(args[1]));
                person.setName(null);
                person.setSex(null);
                person.setBirthDate(null);


            }
            break;

            case "-i" : {
                Person person = allPeople.get(Integer.parseInt(args[1]));
                String s;
                if (person.getSex().equals(Sex.MALE)) s = "м";
                else s = "ж";
                System.out.println(person.getName() + " " + s + " " + date.format(person.getBirthDate()));
            }

            break;
        }
    }
}
