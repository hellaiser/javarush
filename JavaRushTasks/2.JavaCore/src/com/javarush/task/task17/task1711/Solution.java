package com.javarush.task.task17.task1711;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2
*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) {
        SimpleDateFormat date = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        SimpleDateFormat nDate = new SimpleDateFormat("dd/MM/yyyy");
        switch (args[0]) {
            case "-c": synchronized (allPeople){
                for(int i = 1; i < (args.length - 2); i += 3) {
                    try {
                        if (args[i + 1].contains("м")) allPeople.add(Person.createMale(args[i], nDate.parse(args[i + 2])));
                        else allPeople.add(Person.createFemale(args[i], nDate.parse(args[i + 2])));
                        System.out.println((allPeople.size() - 1));
                    } catch (ParseException exc) {}
                }
            }
            break;

            case "-u" : synchronized (allPeople) {
                for(int i = 1; i < (args.length - 3); i += 4) {
                    Person person = allPeople.get(Integer.parseInt(args[i]));
                    person.setName(args[i + 1]);
                    if(args[i + 2].contains("м")) person.setSex(Sex.MALE);
                    else person.setSex(Sex.FEMALE);
                    try {
                        person.setBirthDate(nDate.parse(args[i + 3]));//start here - начни тут
                    }

                    catch (ParseException exc) {}
                }
            }

            break;

            case "-d" : synchronized (allPeople) {
                for(int i = 1; i < args.length; i++) {
                    Person person = allPeople.get(Integer.parseInt(args[i]));
                    person.setName(null);
                    person.setSex(null);
                    person.setBirthDate(null);
                }



            }
            break;

            case "-i" : synchronized (allPeople) {
                for(int i = 1; i < args.length; i++) {
                    Person person = allPeople.get(Integer.parseInt(args[i]));
                    String s;
                    if (person.getSex().equals(Sex.MALE)) s = "м";
                    else s = "ж";
                    System.out.println(person.getName() + " " + s + " " + date.format(person.getBirthDate()));
                }

            }

            break;
        }//start here - начни тут
    }
}
