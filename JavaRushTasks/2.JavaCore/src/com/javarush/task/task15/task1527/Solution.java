package com.javarush.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Парсер реквестов
*/

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        s = s.substring(s.indexOf("?") + 1);
        int count = s.length() - s.replace("&", "").length();

        String sObj = null;
        for (int i = 0; i < count; i ++){
            String newS = s.substring(0, s.indexOf("&"));
            s = s.substring(s.indexOf("&") + 1);
            if(newS.contains("obj")) {
                sObj = newS.substring(s.indexOf("="));
            }
            if(newS.contains("=")){
                System.out.print(newS.substring(0, newS.indexOf("=")) + " ");
            }
            else System.out.print(newS + " ");
        }

        System.out.println(s.substring(0, s.indexOf("=")));
        if (sObj != null) {
            try {
                double d = Double.parseDouble(sObj);
                alert(d);
            }

            catch (NumberFormatException exc) {
                alert(sObj);
            }
        }
    }

    public static void alert(double value) {
        System.out.println("double: " + value);
    }

    public static void alert(String value) {
        System.out.println("String: " + value);
    }
}
